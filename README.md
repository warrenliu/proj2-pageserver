A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

# Author

* Yiran Liu
* yiranl@uoregon.edu

# Use

```
docker build -t <name> .
```
```
docker run -d -p 5000:5000 <name>
```
